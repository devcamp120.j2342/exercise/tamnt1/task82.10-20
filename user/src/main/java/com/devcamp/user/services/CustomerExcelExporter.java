package com.devcamp.user.services;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import com.devcamp.user.models.Customer;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;

@Service
public class CustomerExcelExporter extends AbstractExporter {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public CustomerExcelExporter() {
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Customers");
        XSSFRow row = sheet.createRow(0);

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        cellStyle.setFont(font);

        createCell(row, 0, "ID", cellStyle);
        createCell(row, 1, "Last Name", cellStyle);
        createCell(row, 2, "First Name", cellStyle);
        createCell(row, 3, "Phone Number", cellStyle);
        createCell(row, 4, "Address", cellStyle);
        createCell(row, 5, "City", cellStyle);
        createCell(row, 6, "State", cellStyle);
        createCell(row, 7, "Postal Code", cellStyle);
        createCell(row, 8, "Country", cellStyle);
        createCell(row, 10, "Credit Limit", cellStyle);

    }

    private void createCell(XSSFRow row, int columnIndex, Object value, CellStyle style) {
        XSSFCell cell = row.createCell(columnIndex);
        sheet.autoSizeColumn(columnIndex);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }

        cell.setCellStyle(style);
    }

    public void export(List<Customer> customers, HttpServletResponse response) throws IOException {
        super.setResponseHeader(response, "application/octet-stream", ".xlsx");

        writeHeaderLine();
        writeDataLines(customers);

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

    }

    private void writeDataLines(List<Customer> customerList) {
        int rowIndex = 1;

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        cellStyle.setFont(font);

        for (Customer customer : customerList) {
            XSSFRow row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            createCell(row, columnIndex++, customer.getId(), cellStyle);
            createCell(row, columnIndex++, customer.getLastName(), cellStyle);
            createCell(row, columnIndex++, customer.getFirstName(), cellStyle);
            createCell(row, columnIndex++, customer.getPhoneNumber(), cellStyle);
            createCell(row, columnIndex++, customer.getAddress(), cellStyle);
            createCell(row, columnIndex++, customer.getCity(), cellStyle);
            createCell(row, columnIndex++, customer.getState(), cellStyle);
            createCell(row, columnIndex++, customer.getPostalCode(), cellStyle);
            createCell(row, columnIndex++, customer.getCountry(), cellStyle);
            createCell(row, columnIndex++, customer.getCreditLimit(), cellStyle);
        }
    }
}
