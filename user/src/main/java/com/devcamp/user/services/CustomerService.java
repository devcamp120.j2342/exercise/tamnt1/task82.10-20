package com.devcamp.user.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import com.devcamp.user.models.Customer;
import com.devcamp.user.repository.CustomerRepository;
import com.devcamp.user.response.CustomerPerCountry;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getAllCustomers(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Customer> customerPage = customerRepository.findAll(pageable);
        return customerPage.getContent();
    }

    public Customer getCustomerById(int id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        return optionalCustomer.orElse(null);
    }

    public Customer createCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(int id, Customer customer) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if (optionalCustomer.isPresent()) {
            Customer existingCustomer = optionalCustomer.get();
            existingCustomer.setLastName(customer.getLastName());
            existingCustomer.setFirstName(customer.getFirstName());
            existingCustomer.setPhoneNumber(customer.getPhoneNumber());
            existingCustomer.setAddress(customer.getAddress());
            existingCustomer.setCity(customer.getCity());
            existingCustomer.setState(customer.getState());
            existingCustomer.setPostalCode(customer.getPostalCode());
            existingCustomer.setCountry(customer.getCountry());
            return customerRepository.save(existingCustomer);
        } else {
            return null;
        }
    }

    public void deleteCustomer(int id) {
        customerRepository.deleteById(id);
    }

    public List<Object[]> getCustomerPerCountry() {
        System.out.println(customerRepository.findCustomerPerCountry());
        return customerRepository.findCustomerPerCountry();
    }
}
