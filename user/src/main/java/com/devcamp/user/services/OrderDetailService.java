package com.devcamp.user.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.models.OrderDetail;
import com.devcamp.user.repository.OrderDetailRepository;

@Service
public class OrderDetailService {
    private final OrderDetailRepository orderDetailRepository;

    public OrderDetailService(OrderDetailRepository orderDetailRepository) {
        this.orderDetailRepository = orderDetailRepository;
    }

    public List<OrderDetail> getAllOrderDetails(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<OrderDetail> orderDetailPage = orderDetailRepository.findAll(pageable);
        return orderDetailPage.getContent();
    }

    public OrderDetail getOrderDetailById(int id) {
        Optional<OrderDetail> optionalOrderDetail = orderDetailRepository.findById(id);
        return optionalOrderDetail.orElse(null);
    }

    public OrderDetail createOrderDetail(OrderDetail orderDetail) {
        return orderDetailRepository.save(orderDetail);
    }

    public OrderDetail updateOrderDetail(int id, OrderDetail orderDetail) {
        Optional<OrderDetail> optionalOrderDetail = orderDetailRepository.findById(id);
        if (optionalOrderDetail.isPresent()) {
            OrderDetail existingOrderDetail = optionalOrderDetail.get();
            existingOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());
            existingOrderDetail.setPriceEach(orderDetail.getPriceEach());
            return orderDetailRepository.save(existingOrderDetail);
        } else {
            return null;
        }
    }

    public void deleteOrderDetail(int id) {
        orderDetailRepository.deleteById(id);
    }
}
