package com.devcamp.user.services;

import com.devcamp.user.models.Employee;
import com.devcamp.user.repository.EmployeeRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Employee> employeePage = employeeRepository.findAll(pageable);
        return employeePage.getContent();
    }

    public Employee getEmployeeById(int id) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        return optionalEmployee.orElse(null);
    }

    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Employee updateEmployee(int id, Employee employee) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if (optionalEmployee.isPresent()) {
            Employee existingEmployee = optionalEmployee.get();
            existingEmployee.setLastName(employee.getLastName());
            existingEmployee.setFirstName(employee.getFirstName());
            existingEmployee.setEmail(employee.getEmail());
            existingEmployee.setExtension(employee.getExtension());
            existingEmployee.setJobTitle(employee.getJobTitle());
            existingEmployee.setOfficeCode(employee.getOfficeCode());
            return employeeRepository.save(existingEmployee);
        } else {
            return null;
        }
    }

    public void deleteEmployee(int id) {
        employeeRepository.deleteById(id);
    }
}
