package com.devcamp.user.exportData;

import java.util.List;
import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.devcamp.user.models.Product;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;

public class ProductExcelExporter extends AbstractExporter {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public ProductExcelExporter() {
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Products");
        XSSFRow row = sheet.createRow(0);

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        cellStyle.setFont(font);

        createCell(row, 0, "ID", cellStyle);
        createCell(row, 1, "Product Code", cellStyle);
        createCell(row, 2, "Product Name", cellStyle);
        createCell(row, 3, "Product Description", cellStyle);
        createCell(row, 4, "Product Scale", cellStyle);
        createCell(row, 5, "Product Vendor", cellStyle);
        createCell(row, 6, "Quantity in Stock", cellStyle);
        createCell(row, 7, "Buy Price", cellStyle);
    }

    private void createCell(XSSFRow row, int columnIndex, Object value, CellStyle style) {
        XSSFCell cell = row.createCell(columnIndex);
        sheet.autoSizeColumn(columnIndex);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }

        cell.setCellStyle(style);
    }

    public void export(List<Product> products, HttpServletResponse response) throws IOException {
        super.setResponseHeader(response, "application/octet-stream", ".xlsx");

        writeHeaderLine();
        writeDataLines(products);

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }

    private void writeDataLines(List<Product> productList) {
        int rowIndex = 1;

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        cellStyle.setFont(font);

        for (Product product : productList) {
            XSSFRow row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            createCell(row, columnIndex++, product.getId(), cellStyle);
            createCell(row, columnIndex++, product.getProductCode(), cellStyle);
            createCell(row, columnIndex++, product.getProductName(), cellStyle);
            createCell(row, columnIndex++, product.getProductDescription(), cellStyle);
            createCell(row, columnIndex++, product.getProductScale(), cellStyle);
            createCell(row, columnIndex++, product.getProductVendor(), cellStyle);
            createCell(row, columnIndex++, product.getQuantityInStock(), cellStyle);
            createCell(row, columnIndex++, product.getBuyPrice(), cellStyle);
        }
    }
}
