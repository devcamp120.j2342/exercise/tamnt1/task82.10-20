package com.devcamp.user.exportData;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.devcamp.user.models.Employee;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;

public class EmployeeExcelExporter extends AbstractExporter {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public EmployeeExcelExporter() {
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employees");
        XSSFRow row = sheet.createRow(0);

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        cellStyle.setFont(font);

        createCell(row, 0, "ID", cellStyle);
        createCell(row, 1, "Last Name", cellStyle);
        createCell(row, 2, "First Name", cellStyle);
        createCell(row, 3, "Extension", cellStyle);
        createCell(row, 4, "Email", cellStyle);
        createCell(row, 5, "Office Code", cellStyle);
        createCell(row, 6, "Report To", cellStyle);
        createCell(row, 7, "Job Title", cellStyle);
    }

    private void createCell(XSSFRow row, int columnIndex, Object value, CellStyle style) {
        XSSFCell cell = row.createCell(columnIndex);
        sheet.autoSizeColumn(columnIndex);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }

        cell.setCellStyle(style);
    }

    public void export(List<Employee> employees, HttpServletResponse response) throws IOException {
        super.setResponseHeader(response, "application/octet-stream", ".xlsx");

        writeHeaderLine();
        writeDataLines(employees);

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }

    private void writeDataLines(List<Employee> employeeList) {
        int rowIndex = 1;

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        cellStyle.setFont(font);

        for (Employee employee : employeeList) {
            XSSFRow row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            createCell(row, columnIndex++, employee.getId(), cellStyle);
            createCell(row, columnIndex++, employee.getLastName(), cellStyle);
            createCell(row, columnIndex++, employee.getFirstName(), cellStyle);
            createCell(row, columnIndex++, employee.getExtension(), cellStyle);
            createCell(row, columnIndex++, employee.getEmail(), cellStyle);
            createCell(row, columnIndex++, employee.getOfficeCode(), cellStyle);
            createCell(row, columnIndex++, employee.getReportTo(), cellStyle);
            createCell(row, columnIndex++, employee.getJobTitle(), cellStyle);
        }
    }
}
