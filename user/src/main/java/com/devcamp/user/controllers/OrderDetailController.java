package com.devcamp.user.controllers;

import com.devcamp.user.exportData.OrderDetailExcelExporter;
import com.devcamp.user.models.OrderDetail;
import com.devcamp.user.repository.OrderDetailRepository;
import com.devcamp.user.services.OrderDetailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class OrderDetailController {
    private final OrderDetailService orderDetailService;
    private final OrderDetailRepository orderDetailRepository;

    public OrderDetailController(OrderDetailService orderDetailService, OrderDetailRepository orderDetailRepository) {
        this.orderDetailService = orderDetailService;
        this.orderDetailRepository = orderDetailRepository;
    }

    @GetMapping("/order-details")
    public ResponseEntity<List<OrderDetail>> getAllOrderDetail(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<OrderDetail> orders = orderDetailService.getAllOrderDetails(page, size);
            return ResponseEntity.ok(orders);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/order-details/{id}")
    public ResponseEntity<OrderDetail> getOrderDetailById(@PathVariable int id) {
        OrderDetail orderDetail = orderDetailService.getOrderDetailById(id);
        if (orderDetail != null) {
            return ResponseEntity.ok(orderDetail);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/order-details")
    public ResponseEntity<OrderDetail> createOrderDetail(@RequestBody OrderDetail orderDetail) {
        OrderDetail createdOrderDetail = orderDetailService.createOrderDetail(orderDetail);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdOrderDetail);
    }

    @PutMapping("/order-details/{id}")
    public ResponseEntity<OrderDetail> updateOrderDetail(@PathVariable int id, @RequestBody OrderDetail orderDetail) {
        OrderDetail updatedOrderDetail = orderDetailService.updateOrderDetail(id, orderDetail);
        if (updatedOrderDetail != null) {
            return ResponseEntity.ok(updatedOrderDetail);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/order-details/{id}")
    public ResponseEntity<Void> deleteOrderDetail(@PathVariable int id) {
        orderDetailService.deleteOrderDetail(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/export/order-details/excel")
    public void exportOrderDetailsToExcel(HttpServletResponse response) throws IOException {

        List<OrderDetail> orderDetails = new ArrayList<>();
        orderDetailRepository.findAll().forEach(orderDetails::add);

        OrderDetailExcelExporter exporter = new OrderDetailExcelExporter();
        exporter.export(orderDetails, response);
    }

}
